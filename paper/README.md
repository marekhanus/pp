# Elixir

Elixir je funkcionální programovací jazyk, který běží na BEAM virtuálním stroji. Elixir byl založen v roce 2011 a je postaven na Erlangu, který existuje již od roku 1986. Samotný Erlang byl vytvořený pro použití v telefonních ústřednách, tedy s důrazem realtime komunikaci, odolnost proti chybám, distribuovanost a jednoduchost při nasazení nových verzí bez výpadku.

Elixir je využit v množství aplikací využívající realtime komunikaci, tedy Discord, Pinterest a dalších. Erlang dále např. u komunikátoru WhatsApp.

Funkce

- překlad do bytekódu
- může být kombinován s Erlang díky kompatibilitě bytekódu
- dokumentace podobné Python docstring

### Instalace

macOS

```
brew install elixir
```

Alpine Linux

```
apk add elixir
```

CentOS

```
yum install elixir
```

Ubuntu

```
apt-get install esl-erlang elixir
```

Příkaz ke spuštění aplikace

```
elixir mujsoubor.esx
```
