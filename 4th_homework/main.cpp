// OMP.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <stdlib.h>
#include <omp.h>
#include <cmath>

# define M_PI           3.14159265358979323846

int main()
{
    omp_set_num_threads(4);

    int all = 1;
    for (int k = 0; k < 8; k++) {
        all = all * 10;
        int inside = 0;

#pragma omp parallel
        {
            std::srand(std::time(0)+omp_get_thread_num());
            int inner_count = 0;
            int count_i = 0;
#pragma omp  for
            for (int i = 0; i < all; i++) {
                double x = ((double)std::rand()) / RAND_MAX;
                double y = ((double)std::rand()) / RAND_MAX;

                if (sqrt(x * x + y * y) <= 1) {
                    inner_count++;
                }
                count_i = i;
            }
#pragma omp critical
            {
                inside += inner_count;
                printf("%d -> %d %d %d\n", omp_get_thread_num() , inner_count , all, count_i);
            }
        }
        printf("%d %f %f\n", all, (double)inside / all * 4, M_PI);
    }

}
/* [i,k][k,j]
double** multiply(double* a, double** b) {
    double** c;
    for (int i = 0; ;) {
        for (int j = 0; ;) {
            for (int k = 0; ;) {
                c[i, j] += a[i*vel_radku+ k] * b[k, j];
            }
        }
    }
    return c;
}*/
