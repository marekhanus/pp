-- sum
sum :: Int -> Int -> Int
sum x y = x + y

-- Function that computes n-th number in Fibonacci sequence.
fib :: Int -> Int
fib 0 = 1
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

-- another fibonacci
tmp 0 a b = a
tmp n a b = tmp (n-1) b (a+b)

-- optimized another fibonacci
fib' n = tmp n 1 1 where
    tmp 0 a b = a
    tmp n a b = tmp (n-1) b (a+b)

-- array sum
sumIt :: [Int] -> Int
sumIt [] = 0
sumIt (x:xs) = x + sumIt xs

-- ### practise

-- Function that checks if a year is a leap-year (divisible without remainder by 4 and it is not divisible by 100. If it is divisible by 400, it is a leap-year).
leapYear :: Int -> Bool
leapYear x = mod x 4 == 0 && mod x 100 /= 0 || mod x 400 == 0

leapYear' :: Int -> Bool
leapYear' x | mod x 400 == 0 = True
            | mod x 100 == 0 = False
            | otherwise = mod x 4 == 0

-- Implement a function that computes greatest common divider for two given numbers.
gcd' :: Int -> Int -> Int
gcd' x y | x == y = x
         | x > y = gcd' x (y-x)
         | otherwise = gcd' (y-x) y

-- Create a function that returns the first element in the list.
getHead :: [a] -> a
getHead (x:xs) = x

-- Create a function that returns the last element in the list.
getLast :: [a] -> a
-- getLast (x:xs) | xs == [] = x
--                | otherwise = getLast x s
getLast [x] = x
getLast (_:xs) = getLast xs

-- Create a function that checks if an element is a member of the list.
isElement :: Eq a => a -> [a] -> Bool
isElement n [] = False
isElement n (x:xs) | n /= x = isElement n xs
                   | otherwise = True

-- Create a function that merge two lists into one list.
combine :: [a] -> [a] -> [a]
combine [] y = y
combine (x:xs) y = x : combine xs y
